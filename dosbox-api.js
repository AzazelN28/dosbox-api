const net = require('net')
const { spawn } = require('child_process')
const path = require('path')
const crypto = require('crypto')
const https = require('https')
const config = require('./config')

const credentials = {
  userName: process.argv[2],
  userToken: process.argv[3]
}

function sha1(data) {
  return crypto.createHash('sha1').update(data).digest('hex')
}

function hasStart(command) {
  return /\[/.test(command)
}

function hasEnd(command) {
  return /\]/.test(command)
}

function isPartial(command) {
  return !isComplete(command)
}

function isComplete(command) {
  return /^\[(.*?)\]$/.test(command)
}

function parseCommand(command) {
  return command.replace(/^\[|\]$/g, '').split(' ')
}

function parseCommands(data, commands = []) {
  let count = 0, offset = -1, previousOffset = 0
  while ((offset = data.indexOf('[', offset + 1)) > -1) {
    const command = data.substr(previousOffset, offset)
    if (command !== '') {
      if (isComplete(command)) {
        commands.push(command)
      } else if (isPartial(command) && count === 0) {
        if (hasStart(command)) {
          commands.push(command)
        } else if (hasEnd(command)) {
          if (!hasStart(commands[commands.length - 1]) || commands.length === 0) {
            throw new Error('Invalid command queue')
          }
          commands[commands.length - 1] += command
        }
      }
      count++
    }
    previousOffset = offset
  }

  const command = data.substr(previousOffset)
  if (isPartial(command) && count === 0) {
    if (hasStart(command)) {
      commands.push(command)
    } else {
      if (!hasStart(commands[commands.length - 1]) || commands.length === 0) {
        throw new Error('Invalid command queue')
      }
      commands[commands.length - 1] += command
    }
  } else {
    commands.push(command)
  }
  return commands
}

function IwantMyTrophy(credentials, trophyId = 116845) {
  return new Promise((resolve, reject) => {
    const url = `https://api.gamejolt.com/api/game/v1_2/trophies/add-achieved/?game_id=${config.gameId}&username=${credentials.userName}&user_token=${credentials.userToken}&trophy_id=${trophyId}`
    const signature = sha1(url + config.privateKey)
    https.get(url + '&signature=' + signature, (res) => {
      let rawData = ''
      res.on('data', (chunk) => { rawData += chunk })
      res.on('end', () => {
        try {
          const parsedData = JSON.parse(rawData)
          console.log(parsedData)
        } catch (e) {
          console.error(e.message)
        }
      })
    }).on('error', (error) => {
      console.log(error)
    })
  })
}

const commands = []

const server = net.createServer((client) => {
  console.log('DOSBox client connected')
  client.on('data', (data) => {
    console.log('data', data.toString('ascii'))
    // Parseamos los comandos y los
    // añadimos a nuestra lista de comandos.
    console.log(parseCommands(data.toString('ascii').replace(/\r\n/g, ''), commands))

    // Mientras que la lista de comandos sea mayor que 0
    // y tenga comandos completos.
    while (commands.length > 0 && isComplete(commands[0])) {
      const command = commands.shift()
      console.log('Executing command')
      const [commandName, commandArgs] = parseCommand(command)
      console.log('Command: ', commandName, commandArgs)
      switch (commandName) {
        case 'TROP':
          IwantMyTrophy(credentials)
          break
      }
    }

  })
})
server.listen(5000)

const dos = spawn('dosbox', ['-conf', 'dosbox-api.conf', '.'])
dos.on('close', (code) => {
  process.exit(code)
})
