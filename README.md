# DOSBox GameJolt API

## ¿Cómo funciona esto?

Es bastante sencillo, por una parte con Node.js levantamos un servidor
que está escuchando en el puerto 5000. Por otra parte DOSBox será el
cliente que se conecte a nuestro servidor Node.js a través del puerto
5000 con la opción `nullmodem` de los puertos de serie de DOSBox.

![Diagrama](docs/diagrama.dot.png)

En Node.JS he implementado un pequeño protocolo para que pueda juntar
lo que he llamado "comandos". ¿Por qué? Porque el puerto serie fragmenta
muchísimo la información y era necesario atajar ese problema. El protocolo
es bien sencillo, cualquier cosa entre corchetes es un comando.

```
echo [TROP 123456] > COM1
```

Este comando desbloquearía el trofeo 123456 si existiese.

Para poder ejecutar este código necesitas un Game Token de GameJolt y pasarlo
como parámetros de la app de Node.js:

```
node dosbox-api.js <usuario> <token de usuario>
```

## Problemas

### Sin respuesta

Uno de los problemas que tendría esta implementación es que el juego
en DIV no podría obtener ningún tipo de respuesta, ya que para hacer
las llamadas a la API, el juego en DIV debería hacer esto:

```
system("echo [TROP 123456] > COM1");
```

Made with :heart: by [AzazelN28](https://gitlab.com/AzazelN28)
